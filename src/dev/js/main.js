//Фиксированная шапка

let header = document.querySelector('.b-header');
if(header !== null) {
    window.addEventListener('scroll', function() {
        if(document.documentElement.scrollTop > 150) {
            header.classList.add('b-header--fixed');
        } else {
            header.classList.remove('b-header--fixed');
        }
    })
}

//Бургер меню

let burger = document.querySelector('.b-burger'),
    nav = document.querySelector('.b-nav');
burger.addEventListener('click', function() {
    nav.classList.toggle('b-nav--open');
    burger.classList.toggle('b-burger--open');
})